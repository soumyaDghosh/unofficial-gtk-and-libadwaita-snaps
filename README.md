# Unofficial GTK and Libadwaita Snaps Repository
![Unofficial GTK and Libadwaita Snaps](https://snapcraft.io/static/images/badges/en/snap-store-black.svg)

Welcome to the Unofficial GTK and Libadwaita Snaps Repository! This repository is dedicated to providing a platform for users to file issues and provide feedback related to the GTK and Libadwaita Snaps.

## For which GTK and Libadwaita apps?

Currently I have around 25 such apps in the [Snap Store](https://snapcraft.io/store). This repository is a unified place to report bugs for all those snaps. You can get the list of snaps [here](https://snapcraft.io/publisher/soumyadghosh).

## How to Use This Repository

### Filing Issues

If you encounter any issues or bugs while using these snaps maintained by me unofficially, I encourage you to file an issue in this repository. To do so, follow these steps:

1. Click on the "Issues" tab at the top of this page.

2. Click the green "New Issue" button.

3. Provide a descriptive title and detailed description of the issue you are experiencing. Include any relevant information such as error messages, steps to reproduce the issue, and the version of the snap you are using.

4. Add appropriate labels or assignees to help us categorize and address the issue more effectively.

5. Click "Submit New Issue" to create the issue.

Our team will review your issue and work towards resolving it as soon as possible.

### Providing Feedback

We value your feedback and suggestions for improving the GTK and Libadwaita Snaps. If you have any ideas, feature requests, or general feedback, please feel free to create a new issue using the steps mentioned above.

### Participating in Discussions

You can also engage with the community and participate in discussions related to the GTK and Libadwaita Snaps. You can find ongoing discussions in the "Issues" tab. Feel free to comment on existing issues, provide solutions, or share your insights and experiences.

## Get Started Now!

To begin filing issues or providing feedback, please visit the [Issues](https://gitlab.com/soumyaDghosh/unofficial-gtk-and-libadwaita-snaps/-/issues) page of this repository.

Let's work together to improve the GTK and Libadwaita Snaps and create a better user experience for everyone.

Happy snap users! 📸✨
