## Issue Summary

Please provide a brief summary of the issue you are experiencing with the Blanket snap.

## Steps to Reproduce

1. 
2. 
3. 

## Expected Behavior

Please describe what you expected to happen.

## Actual Behavior

Please describe what actually happened.

## Additional Information

Please provide any additional information or details that may be helpful in resolving the issue.

## Environment

### Snap Information

Paste the output of `snap info blanket`

### Snap Connections

Paste the output of `snap connections blanket`

### Snapd Version

Paste the output of `snap --version`

## Screenshots / Logs(if available)

